#include <stdio.h>
#include <stdlib.h>

// Estrutura de um nodo: pode representar uma raiz, nodo intermediário ou
// folha, dependendo se possui irmãos, filhos ou pai.
typedef struct comp_tree_t comp_tree_t;

struct comp_tree_t {
    comp_tree_t* nextSibling; 
    comp_tree_t* youngestChild; 
    comp_tree_t* father;
    int data;
}; 

// Assinaturas das funções
comp_tree_t* createNode(int data);
void appendChild(comp_tree_t* t, comp_tree_t* newChild);
void removeNode (comp_tree_t* node);
int searchTree (comp_tree_t* t, int data);
void printTree(comp_tree_t* node);

