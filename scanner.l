/* Compiladores, Turma B, Grupo: André Luz Gonçalves, Arthur Foscarini, Gabriel Delavald */

%{

#include "tokens.h"
#include "main.c"
%}

/* Expressões Regulares da Linguagem */


WHITESPACE  [ \t\v\f]*
ID          [_a-zA-Z][_a-zA-Z0-9]*
LIT_INT         [0-9]+
LIT_FLOAT       [0-9]+"."[0-9]+
LIT_CHAR        ("\'")[^\']?("\'")
LIT_STRING      ("\"")[^\"]*("\"")


/* Regras de Tradução*/
%%

"int" 			{ return TK_PR_INT; }
"float" 		{ return TK_PR_FLOAT; }
"bool" 			{ return TK_PR_BOOL; }
"char" 			{ return TK_PR_CHAR; }
"string" 		{ return TK_PR_STRING; }
"if" 			{ return TK_PR_IF; }
"then" 			{ return TK_PR_THEN; }
"else" 			{ return TK_PR_ELSE; }
"while" 		{ return TK_PR_WHILE; }
"do" 			{ return TK_PR_DO; }
"input" 		{ return TK_PR_INPUT; }
"output" 		{ return TK_PR_OUTPUT; }
"return" 		{ return TK_PR_RETURN; }
"<="			{ return OPERATOR_LE; }
">="			{ return OPERATOR_GE; }
"=="			{ return OPERATOR_EQ; }
"!="			{ return OPERATOR_NE; }	
"&&"			{ return OPERATOR_AND; }
"||"			{ return OPERATOR_OR; }
","|";"|":"|")"|"("|"["|"]"|"{"|"}"|"+"|"-"|"*"|"/"|"<"|">"|"="|"!"|"&"|"$"  { return yytext[0]; }
"\n" 			{ lineNumber++; }
. 				{ return TOKEN_ERRO;}
%%
