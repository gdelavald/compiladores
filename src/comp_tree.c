#include "comp_tree.h"


// Cria um novo nodo com um dado especificado.
// Entrada: dado
// Saída: nodo com o dado
comp_tree_t* createNode(int data) {
    comp_tree_t* newNode = (comp_tree_t*)malloc(sizeof(comp_tree_t));
    newNode->nextSibling = NULL;
    newNode->youngestChild = NULL;
    newNode->father = NULL;
    newNode->data = data;
    return newNode;
}

//imprime a arvore
void printTree(comp_tree_t* node) {
    comp_tree_t* p;
    printf("%i\n",node->data);
    for (p=node->youngestChild; p!=NULL; p=p->nextSibling) {
	printTree(p);
    }
}

// Isere um nodo(sub-arvore) em uma arvore
void appendChild(comp_tree_t* t, comp_tree_t* newNode) {
    newNode->nextSibling = t->youngestChild;
    t->youngestChild = newNode;
    newNode->father = t;
}

int searchTree (comp_tree_t* t, int data) {
    comp_tree_t* p;
    if (t->data==data)
	return 1;
    else {
	for (p=t->youngestChild; p!=NULL; p=p->nextSibling) {
	    if (searchTree(p,data))
		return 1;
	}
    }
    return 0;
}

//Remove o nodo da arvore e todos o filhos dele
void removeNode (comp_tree_t* node) {
    comp_tree_t* f = node->father;
    f->youngestChild = node->nextSibling;
    comp_tree_t* p = node->youngestChild;
    while (p!=NULL) {
	comp_tree_t* t = p->nextSibling;
	removeNode(p);
	p = t;
    }
    free(node);
}

void test_tree () {
    // Estrutura da árvore:
    //                 100
    //         30       20       10
    //      3     13              1
    //    11 12 
    comp_tree_t* cem = createNode(100);
    comp_tree_t* dez = createNode(10);
    comp_tree_t* vinte = createNode(20);
    comp_tree_t* trinta = createNode(30);
    comp_tree_t* um = createNode(1);
    comp_tree_t* dois = createNode(2);
    comp_tree_t* tres = createNode(3);        
    comp_tree_t* onze = createNode(11);
    comp_tree_t* doze = createNode(12);
    comp_tree_t* treze = createNode(13);        
    appendChild(cem,dez);
    appendChild(cem,vinte);
    appendChild(cem,trinta);
    appendChild(dez,um);
    appendChild(trinta,tres);
    appendChild(trinta,treze);
    appendChild(tres,onze);
    appendChild(tres,doze);
    	
    
    printf("Teste de appendChild():\n");
    printf("%d\n",cem->youngestChild->youngestChild->data); // imprime o filho do primeiro filho de 100, que é 3
    
    printTree(cem);
    
    if(searchTree(cem, 30))
	printf("found\n");
    else
	printf("not found\n");
	
    if(searchTree(cem, 24))
	printf("found\n");
    else
	printf("not found\n");
    removeNode(trinta);
    printTree(cem);
}